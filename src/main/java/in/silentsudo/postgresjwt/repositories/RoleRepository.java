package in.silentsudo.postgresjwt.repositories;

import in.silentsudo.postgresjwt.persistence.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface RoleRepository extends CrudRepository<Role, UUID> {
}
