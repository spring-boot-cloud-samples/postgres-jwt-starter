package in.silentsudo.postgresjwt.repositories;

import in.silentsudo.postgresjwt.persistence.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface LoginRepository extends CrudRepository<User, UUID> {

    Optional<User> findByUsername(String username);
}
