package in.silentsudo.postgresjwt.controllers;

import in.silentsudo.postgresjwt.config.security.JwtUtils;
import in.silentsudo.postgresjwt.models.http.LoginRequest;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping(value = "/auth")
@AllArgsConstructor
public class AuthController {
    private final JwtUtils jwtUtils;
    private final AuthenticationManager authenticationManager;

    @GetMapping(value = "/login")
    public Map<String, String> get() {
        return Map.of("status", HttpStatus.NOT_ACCEPTABLE.toString());
    }

    @PostMapping(value = "/login")
    public Map<String, String> post(@RequestBody LoginRequest loginRequest) {
        final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                loginRequest.getPassword()));
        return Map.of(
                "status", authentication.isAuthenticated() ? HttpStatus.OK.name() : HttpStatus.FORBIDDEN.name(),
                "authentication", Boolean.valueOf(authentication.isAuthenticated()).toString(),
                "roles", authentication.getAuthorities().toString(),
                "token", jwtUtils.generateJwtToken(authentication)
        );
    }
}
