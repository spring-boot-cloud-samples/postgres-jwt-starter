package in.silentsudo.postgresjwt.controllers;

import lombok.AllArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(value = "/profile")
@AllArgsConstructor
public class ProfileController {

    @GetMapping(value = "/")
    public Map<String, Object> get(Authentication authentication) {
        return Map.of(
                "status", authentication.getName(),
                "user", authentication.getPrincipal()
        );
    }

}
