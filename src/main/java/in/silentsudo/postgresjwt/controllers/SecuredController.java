package in.silentsudo.postgresjwt.controllers;

import in.silentsudo.postgresjwt.persistence.User;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping(path = "/secured")
public class SecuredController {
    @GetMapping
    public Map<String, String> get(@AuthenticationPrincipal User user) {
        return Map.of("user", SecurityContextHolder.getContext().getAuthentication().getName(),
                "ap", user != null ? user.getId().toString() : "none");
    }
}
