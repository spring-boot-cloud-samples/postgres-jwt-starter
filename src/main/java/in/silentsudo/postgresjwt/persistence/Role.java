package in.silentsudo.postgresjwt.persistence;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "roles")
public class Role extends Auditable<String> {


    @Enumerated(EnumType.STRING)
    private Name name;

    public Role() {
    }

    public Role(Name name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY)
    private List<User> user;

    public enum Name {
        USER,
        ADMIN
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }


}
