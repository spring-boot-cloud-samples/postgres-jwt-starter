package in.silentsudo.postgresjwt.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@MappedSuperclass
@EntityListeners(AuditingEntityListener.class)
public abstract class Auditable<U> {

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(length = 36)
    private UUID id;

    @CreatedBy
    @Getter
    @Setter
    @JsonIgnore
    protected U createdBy;

    @Getter
    @Setter
    @CreatedDate
//    @JsonIgnore
    @Column(columnDefinition = "TIMESTAMP")
    protected LocalDateTime creationDate;

    @Getter
    @Setter
    @LastModifiedBy
    @JsonIgnore
    protected U lastModifiedBy;


    @Getter
    @Setter
    @LastModifiedDate
    @Column(columnDefinition = "TIMESTAMP")
//    @JsonIgnore
    protected LocalDateTime lastModifiedDate;

    @Getter
    @Setter
    @JsonIgnore
    @Column(name = "is_active")
    protected boolean active = true;

    public UUID getId() {
        return id;
    }
}