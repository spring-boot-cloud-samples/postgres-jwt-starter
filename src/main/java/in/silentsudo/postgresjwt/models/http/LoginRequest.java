package in.silentsudo.postgresjwt.models.http;

import lombok.Data;

@Data
public class LoginRequest {
    private String username;
    private String password;
}
