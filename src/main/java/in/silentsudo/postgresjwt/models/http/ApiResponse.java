package in.silentsudo.postgresjwt.models.http;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiResponse {
    public int statusCode;
    public String message;
}
