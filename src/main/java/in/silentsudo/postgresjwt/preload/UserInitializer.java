package in.silentsudo.postgresjwt.preload;

import in.silentsudo.postgresjwt.persistence.Role;
import in.silentsudo.postgresjwt.persistence.User;
import in.silentsudo.postgresjwt.repositories.LoginRepository;
import in.silentsudo.postgresjwt.repositories.RoleRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service
@AllArgsConstructor
public class UserInitializer {
    private final RoleRepository roleRepository;
    private final LoginRepository loginRepository;
    private final PasswordEncoder passwordEncoder;

    @PostConstruct
    public void preloadUserData() {

        final Role userRole = new Role(Role.Name.USER);
        final Role adminRole = new Role(Role.Name.ADMIN);
        if (roleRepository.count() == 0) {
            roleRepository.save(userRole);
            roleRepository.save(adminRole);
        }

        if (loginRepository.count() == 0) {
            loginRepository.save(new User("ashish", passwordEncoder.encode("ashish"), List.of(userRole, adminRole)));
            loginRepository.save(new User("john", passwordEncoder.encode("john"), List.of(userRole)));
        }
    }
}
