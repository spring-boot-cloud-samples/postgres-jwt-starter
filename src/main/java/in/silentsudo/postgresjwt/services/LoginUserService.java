package in.silentsudo.postgresjwt.services;

import in.silentsudo.postgresjwt.persistence.User;
import in.silentsudo.postgresjwt.repositories.LoginRepository;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class LoginUserService implements UserDetailsService {

    private final LoginRepository loginRepository;

    public LoginUserService(LoginRepository loginRepository) {
        this.loginRepository = loginRepository;
    }

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {

        return loginRepository.findByUsername(username)
                .orElseThrow((Supplier<RuntimeException>) () -> new UsernameNotFoundException(username));
    }
}
