package in.silentsudo.postgresjwt.config.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import in.silentsudo.postgresjwt.filters.JwtAuthenticationFilter;
import in.silentsudo.postgresjwt.models.http.ApiResponse;
import in.silentsudo.postgresjwt.persistence.Role;
import in.silentsudo.postgresjwt.services.LoginUserService;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@AllArgsConstructor
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final ObjectMapper objectMapper;
    private final JwtUtils jwtUtils;
    private final LoginUserService loginUserService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        final List<String> defaultRoles = Arrays.stream(Role.Name.values()).map(Enum::name).collect(Collectors.toUnmodifiableList());
        http = http.cors().and().csrf().disable();
        http = http.httpBasic().disable();
        http = http.formLogin().disable();
        http = http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and();
        http.authorizeRequests()
                .antMatchers(HttpMethod.POST, "/auth/login").permitAll()
                .antMatchers(HttpMethod.GET, "/auth/login").permitAll()
                .antMatchers(HttpMethod.GET, "/profile/**").hasAnyAuthority(defaultRoles.toArray(String[]::new))
                .antMatchers(HttpMethod.GET, "/secured/**").hasAuthority(Role.Name.ADMIN.name())
                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new JwtEntryPointHandler(objectMapper))
                .accessDeniedHandler(new JwtAccessDeniedHandler(objectMapper));

        http.addFilterBefore(new JwtAuthenticationFilter(jwtUtils, loginUserService), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(loginUserService);
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    private static class JwtEntryPointHandler implements AuthenticationEntryPoint {

        private final ObjectMapper objectMapper;

        public JwtEntryPointHandler(ObjectMapper objectMapper) {

            this.objectMapper = objectMapper;
        }

        @Override
        public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
            sendFailureResponse(objectMapper, response, HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
        }
    }

    private static class JwtAccessDeniedHandler implements AccessDeniedHandler {

        private final ObjectMapper objectMapper;

        public JwtAccessDeniedHandler(ObjectMapper objectMapper) {

            this.objectMapper = objectMapper;
        }

        @Override
        public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException {
            sendFailureResponse(objectMapper, response, HttpStatus.FORBIDDEN.value(), HttpStatus.FORBIDDEN.getReasonPhrase());
        }
    }

    private static void sendFailureResponse(ObjectMapper objectMapper, HttpServletResponse response, int code, String message) throws IOException {
        response.setContentType("application/json;charset=UTF-8");
        response.setStatus(code);
        response.getWriter().write(objectMapper.writeValueAsString(new ApiResponse(code, message)));
    }


}
