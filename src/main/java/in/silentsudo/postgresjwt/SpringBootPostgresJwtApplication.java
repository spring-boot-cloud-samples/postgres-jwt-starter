package in.silentsudo.postgresjwt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootPostgresJwtApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootPostgresJwtApplication.class, args);
    }

}
